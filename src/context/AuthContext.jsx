import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import * as auth from "../services/auth_service";
import { token_key } from "../config";
import { AuthContext } from "./useAuth";

export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    auth
      .getUser()
      .then((user) => setUser(user))
      .catch((error) => console.log(error));
  }, []);

  function login(credentials) {
    auth
      .login(credentials)
      .then((user) => {
        setUser(user), setError(null);
      })
      .catch((error) => {
        //console.log(e);
        setError(error);
      });
  }
  function signUp(userData) {
    auth
      .create(userData)
      .then((user) => setUser(user))
      .catch((error) => {
        console.log(error);
      });
  }
  function logout() {
    auth.logout().then(() => {
      sessionStorage.removeItem(token_key);
      setUser(null);
    });
  }

  async function loginGoogle(dataGoogle) {
    const { token, ...user } = await dataGoogle;
    sessionStorage.setItem(token_key, token);
    setUser(user);
  }

  async function loginFacebook(dataGoogle) {
    const { token, ...user } = await dataGoogle;
    sessionStorage.setItem(token_key, token);
    setUser(user);
  }

  return (
    <AuthContext.Provider
      value={{ user, login, logout, signUp, loginGoogle, loginFacebook, error }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthContextProvider.propTypes = {
  children: PropTypes.node,
};
