import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import { decodeToken } from "react-jwt";
import { useAuth } from "../../context/useAuth";
import { clientIdGoogle } from "../../credentials";

const LoginGoogle = () => {
  const { loginGoogle } = useAuth();
  return (
    <GoogleOAuthProvider clientId={clientIdGoogle}>
      <GoogleLogin
        onSuccess={(credentialResponse) => {
          //console.log(credentialResponse);
          const token = credentialResponse.credential; // Obtiene el token JWT
          //console.log(token);
          const decodedToken = decodeToken(token);
          console.log(decodedToken);
          // generar contraseña aleatorio
          const dataGoogle = {
            email: decodedToken.email,
            type: "sesion_gmail",
            name: decodedToken.name,
            token: decodedToken.jti,
          };
          loginGoogle(dataGoogle);
        }}
        onError={() => {
          console.log("Login Failed");
        }}
      />
    </GoogleOAuthProvider>
  );
};

export default LoginGoogle;
