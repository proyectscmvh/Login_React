import ImgLogo from "../../assets/icons/CarbonLogoAnsibleCommunity.svg";

const Logo = () => {
  return (
    <div
      className="flex gap-5 items-center cursor-pointer"
      onClick={() => console.log("click")}
    >
      <img
        className="w-auto h-12 text-white fill-white"
        src={ImgLogo}
        alt="Logo Company"
      />
      <p className="text-3xl sm:text-xl md:text-2xl lg:text-3xl xl:text-3xl font-bold text-white uppercase">
        Company dev
      </p>
    </div>
  );
};

export default Logo;
