import { Container } from "@mui/material";
import IconBarMovile from "./IconBarMovile";
import Logo from "./Logo";
import ItemMenu from "./ItemMenu";
import HeaderMobile from "./HeaderMobile";
import { useState } from "react";
import { useAuth } from "../../context/useAuth";

const Header = () => {
  const [openMenuMobile, setOpenMenuMobile] = useState(false);
  const { user, logout } = useAuth();

  function handleMenuMobile(event) {
    event.preventDefault();
    setOpenMenuMobile(!openMenuMobile);
  }

  function handleLogout(event) {
    event.preventDefault();

    // console.log("click");
    if (user.type) {
      console.log("existe");
      window.location.reload();
    } else {
      logout();
    }
  }

  return (
    <nav className="bg-gradient-to-br from-blue-900 to-blue-400 sticky top-0 z-50 py-2">
      <Container className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
        <div className="relative flex h-16 items-center justify-between">
          <div
            className="flex flex-shrink-0 items-center"
            onClick={handleMenuMobile}
          >
            <IconBarMovile />
          </div>
          <div className="flex flex-col flex-1 items-center justify-center sm:items-center sm:justify-between sm:flex-row">
            <Logo handleMenuMobile={handleMenuMobile} />
            <ItemMenu />
            <div className="relative flex gap-3 items-center">
              <strong className="text-white">Welcome: {user.name}</strong>
              <button
                onClick={handleLogout}
                className="text-white border rounded-md border-slate-600 hover:underline underline-offset-4 bg-blue-950 py-1 px-4"
              >
                Logout
              </button>
            </div>
          </div>
        </div>
      </Container>

      {/* <!-- Mobile menu, show/hide based on menu state. --> */}
      <HeaderMobile isOpen={openMenuMobile} />
    </nav>
  );
};

export default Header;
