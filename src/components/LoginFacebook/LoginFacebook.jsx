import FacebookLogin from "@greatsumini/react-facebook-login";
import IconFacebook from "../../assets/icons/RiFacebookFill.svg";
import { useAuth } from "../../context/useAuth";
import { appIdFacebook } from "../../credentials";

const LoginFacebook = () => {
  const { loginFacebook } = useAuth();

  return (
    <FacebookLogin
      appId={appIdFacebook}
      // onSuccess={(response) => {
      //   console.log("Login Success!", response);
      // }}
      onProfileSuccess={(response) => {
        // console.log("Get Profile Success!", response);
        const dataFacebook = {
          email: response.email,
          type: "sesion_facebook",
          name: response.name,
        };
        loginFacebook(dataFacebook);
      }}
      autoLoad={false}
      onFail={(error) => {
        console.log("Login Failed!", error);
      }}
      className="flex items-center gap-3 justify-center bg-blue-600 text-white text-sm px-6 py-2 rounded hover:bg-blue-700"
    >
      <img src={IconFacebook} alt="icon-face" className="w-5 h-5" />
      <strong>Login With Facebook</strong>
    </FacebookLogin>
  );
};

export default LoginFacebook;
