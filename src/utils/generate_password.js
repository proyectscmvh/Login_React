import securePassword from "secure-random-password";

export const generatePassword = () => {
  const long = 12; // Longitud de la contraseña
  const passwordRandom = securePassword.randomPassword({
    length: long,
    characters:
      securePassword.lowerCase +
      securePassword.upperCase +
      securePassword.digits +
      securePassword.symbols,
    exclude: ['"', "'", "\\"], // Caracteres a excluir si es necesario
  });

  return passwordRandom;
};
