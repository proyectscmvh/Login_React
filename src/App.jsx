import { Navigate, Route, Routes } from "react-router-dom";
import Login from "./pages/Login";
import Home from "./pages/Home";
import PageNotFound from "./pages/PageNotFound";
import { useAuth } from "./context/useAuth";
import PropTypes from "prop-types";

function App() {
  const { user } = useAuth();
  console.log("usuario", user);
  const RequireAuth = ({ children }) => {
    return user ? children : <Navigate to={"/login"} />;
  };
  return (
    <>
      <Routes>
        <Route
          path="/"
          index
          element={
            <RequireAuth>
              <Home />
            </RequireAuth>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </>
  );
}

App.propTypes = {
  children: PropTypes.node,
};

export default App;
