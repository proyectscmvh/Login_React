import Header from "../components/Header/Header";
import { useAuth } from "../context/useAuth";

const Home = () => {
  const { user } = useAuth();

  return (
    <>
      <Header />
      <div className="h-96 flex items-center justify-center flex-grow">
        <h1>Welcome: {user.name}</h1>
      </div>
    </>
  );
};

export default Home;
