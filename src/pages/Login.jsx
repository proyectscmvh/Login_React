import { useEffect, useState } from "react";
import { useAuth } from "../context/useAuth";
import { useNavigate } from "react-router";
import LoginGoogle from "../components/LoginGoogle/LoginGoogle";
import LoginFacebook from "../components/LoginFacebook/LoginFacebook";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { Button, LinearProgress } from "@mui/material";
import { TextField } from "formik-mui";
import CircularProgress from "@mui/material/CircularProgress";

const initialValues = {
  email: "",
  password: "",
};

const FormLogin = {
  Login: Yup.object().shape({
    email: Yup.string().email("Email Invalido").required("Email is required"),
    password: Yup.string()
      .min(6, "too short, minimum length 6")
      .required("password is required"),
    // .matches(/[A-Z]/, "Must contain at least one capital letter")
  }),
};

const Login = () => {
  const [messageError, setMessageError] = useState(false);
  const { user, login, error } = useAuth();
  const navigate = useNavigate();

  function handleLogin(values) {
    login(values);
  }

  useEffect(() => {
    if (user) {
      navigate("/");
    }
  }, [user, navigate]);

  useEffect(() => {
    if (error) {
      setMessageError(true);
      setTimeout(() => {
        setMessageError(false);
      }, 500);
    }
  }, [error]);

  return (
    <div className=" flex items-center justify-center h-screen">
      <div className="max-w-xl mx-auto w-full">
        <div className="flex flex-col gap-2 p-4 border border-blue-700 mx-2 sm:mx-0">
          <h1 className="text-center text-xl">Login</h1>
          <Formik
            initialValues={initialValues}
            onSubmit={(error) => handleLogin(error)}
            validationSchema={FormLogin.Login}
          >
            {({ submitForm }) => (
              <Form className="flex flex-col gap-3">
                <label
                  htmlFor="email"
                  className="block text-sm font-medium leading-3 text-gray-900"
                >
                  Email
                </label>
                <Field
                  component={TextField}
                  name="email"
                  id="email"
                  type="text"
                  placeholder="cverah@mail.com"
                  disabled={messageError}
                  InputProps={{
                    style: {
                      height: "40px",
                    },
                  }}
                />

                <label
                  htmlFor="password"
                  className="block text-sm font-medium leading-3 text-gray-900"
                >
                  Password
                </label>
                <Field
                  component={TextField}
                  name="password"
                  id="password"
                  type="password"
                  disabled={messageError}
                  InputProps={{
                    style: {
                      height: "40px",
                    },
                  }}
                />
                {error && (
                  <p className="text-red-500 text-center">
                    Invalid Credentials
                  </p>
                )}
                {messageError && <LinearProgress />}
                <Button
                  variant="contained"
                  disabled={messageError}
                  onClick={submitForm}
                  style={{
                    textTransform: "uppercase",
                    width: "100%",
                  }}
                >
                  {messageError ? (
                    <CircularProgress
                      color="warning"
                      style={{
                        width: "20px",
                        height: "20px",
                        color: "white",
                      }}
                    />
                  ) : (
                    "Log In"
                  )}
                </Button>
              </Form>
            )}
          </Formik>
          <p className="text-center">Or</p>
          <div className="flex flex-col sm:flex-row justify-center gap-2">
            <LoginGoogle />
            <LoginFacebook />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
