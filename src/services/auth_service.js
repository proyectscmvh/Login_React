import { token_key } from "../config";
import apiFetch from "./api_fetch";

function login(credentials) {
  const user = apiFetch("/login", { body: credentials }).then((data) => {
    const { token, ...user } = data;
    sessionStorage.setItem(token_key, token);
    return user;
  });
  return user;
}

function getUser() {
  const user = apiFetch("/profile").then((u) => {
    const { _token, ...user } = u;
    return user;
  });
  return user;
}

function create(data) {
  const newUser = apiFetch("/users", { body: data }).then((u) => {
    const { token, ...user } = u;
    sessionStorage.setItem(token_key, token);
    return user;
  });
  return newUser;
}

function logout() {
  return apiFetch("/logout", { method: "DELETE" });
}

export { login, create, logout, getUser };
